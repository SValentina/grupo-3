﻿import random

def conjuncion_aleatoria(cantidad_variables,cantidad_clausulas):
    """
    conjuncion_aleatoria(cantidad_variables,cantidad_clausulas)

    Genera una conjunción con valores aleatorios pasando la 
    cantidad de variables y la cantidad de cláusulas.

    Parameters
    ----------
    cantidad_variables : número entero. representa cantidad de variables
    cantidad_clausulas : número entero. representa cantidad de clausulas.

    Returns
    -------
    conjuncion_resultado : lista de tuplas.
        Los elementos de las tuplas(clausulas) son las variables
        Los elementos de la lista son las clausulas.
    
    Example
    -------
    conjuncion_resultado = [(1, n2, 3), (2, n3), (), (1)]

    """
    caracter_negacion = "n"
    conjuncion_resultado = []
    for _ in range(cantidad_clausulas):
        clausula = ()
        lista = []
        for i in range(cantidad_variables):
            es_variable = random.randrange(0,2)
            es_variable_negada = random.randrange(0,2)
            i+=1
            if es_variable == 1:
                if es_variable_negada == 1:
                    lista.append(caracter_negacion+str(i))
                else: 
                    lista.append(str(i))
        clausula=tuple(lista)
        conjuncion_resultado.append(clausula)
    return conjuncion_resultado


def armar_parejas(padres):
    """
    armar_parejas(padres):

    Dado un array de posiciones de padres, arma tuplas que representan 
    las parejas de padres con las que luego se va a hacer el cruce

    Parameters
    ----------
    padres: vector de padres 

    Returns
    -------
    lista de tuplas donde cada tupla es una pareja de padres
    
    Example
    -------
    padres = [1,4,7,5,1,8]
    lista = [(1,4),(7,5),(1,8)]

    """
    lista = []
    j=1
    i=0
    while (j < len(padres)):
        tupla = (padres[i],padres[j])
        lista.append(tupla)
        i+=2
        j+=2
    return lista


def validarArchivosInstancia(f):
    f.seek(0,0)
    i=0
    msg=""
    line=f.readline().split()
    ##Separación de Valores de Instancia
    cantidad_clausulas=int(line[0])
    cantidad_variables=int(line[1])
    cantidad_conjunciones=int(line[2])


    for line in f:
        i+=1
        line_split = line.split()
        if(len(line_split) == cantidad_clausulas):
            for j in range(cantidad_clausulas):
                if abs(int(line_split[j])) > cantidad_variables:
                    return "El valor de la fila " + str(i+1) + ", columna "+ str(j) +" excede el rango de variables declarado en el archivo: " + str(cantidad_variables) 
        else:
            return "No coincide la cantidad de cláusulas informada con las encontradas en el archivo. <br>Informados: " +str(cantidad_clausulas)+ "<br>Se encontraron: " + str(len(line_split)) + " en la línea " + str(i)

    f.seek(0,0)
    cantidad_total_lineas = len(f.readlines())

    if(cantidad_total_lineas-1 != cantidad_conjunciones):
        return "No coincide la cantidad de clausulas con la cantidad de columnas del archivo. <br>Informados: " +str(cantidad_conjunciones)+ "<br>Se encontraron: " + str(cantidad_total_lineas) 

    return msg
