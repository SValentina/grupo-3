from datetime import datetime
import csv
import sys

def generarArchivoResultado(resultados,representacion,problema):
    CARPETA_RESULTADOS_EJECUCION = sys.path[0] + "/resultados/"
    nombreArchivoGenerado = ""
    #########################################################################################################

    resultadosCsv = resultados
    #Generar metodo de buscar nombre para archivo. esto sacar afuera en una funcion en algun momento
    now = datetime.now()
    date_time = now.strftime("%m%d%Y%H%M%S%f")
    print("date and time:",now.strftime("%m-%d-%Y %H:%M:%S:%f"))
    nombreArchivo = representacion +"_"+ problema + date_time + '.csv'
    with open(CARPETA_RESULTADOS_EJECUCION + nombreArchivo, 'w', newline='') as f:
        fieldnames = ['mejor_solucion_inicial', 'peor_solucion_inicial', 'promedio_solucion_inicial','mejor_solucion_final', 'peor_solucion_final', 'promedio_solucion_final','tiempo_total_ejecucion','tiempo_mejor_solucion','cantidad_evaluaciones_funcion_fitness','diferencia_relativa_optimo','error_relativo','total_generaciones','generacion_mejor_solucion','mejor_solucion' , 'optimo' ]
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        for key in resultadosCsv:
            for k, v in resultadosCsv[key].items():
                resultadosCsv[key][k] = float(v)
            writer.writerow(resultados[key])
    nombreArchivoGenerado = "download?filename=" + nombreArchivo

    return nombreArchivoGenerado
