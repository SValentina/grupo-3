//Control de Valores para Ejecución del Problema
function R_pof_checkValues(){
    var alfa_value=$("#valorAlfa").val();
    var function_value=$("input[name='funciones']:checked").val();
    var inf_limit=parseInt($("#limiteInferior").val(),10);
    var sup_limit=parseInt($("#limiteSuperior").val(),10);
    //String de mensaje
    var msg="<ul>";
    if(parseFloat(alfa_value)<=0 || parseFloat(alfa_value)>=1){
        msg+="<li>El Valor de alfa debe ser ente 0,01 y 0,99</li>";
    }
    if(parseInt(function_value)<1 || parseInt(function_value)>6){
        msg +="<li>El parametro relacionado al tipo de función no es correcto</li>";
    }
    msg+="</ul>";
    return msg;
}
//Control de Archivo de Configuración del Problema
function pof_checkProblemConfig(params) {
    var res = "";
    if (parseInt(params[1], 10) < 1 || parseInt(params[1], 10) > 6) {
        res += "<li>El parametro relacionado al tipo de función no es correcto</li>";
    }
    return res;
}