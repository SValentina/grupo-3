$(document).ready(function () {
    //Menu Control
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    //Dependiendo de la opción elegida en el Sidebar, se carga un template u otro
    $("#ProblemaSAT").on("click", function () {
        $("#valoresIniciales").load("/valores_iniciales");
        $("#operadores").load("/Binario");

    });
    $("#ProblemaMinMaxFunciones").on("click", function () {
        $("#valoresIniciales").load("/valores_iniciales");
        $("#operadores").load("/Real");
    });
    $("#ProblemaFSSP").on("click", function () {
        $("#valoresIniciales").load("/valores_iniciales");
        $("#operadores").load("/Permutacion");
    });
    //Cargar Template Modal Carga Rápida
    $("#botonCarga").on("click", function () {
        $("#cargaRapida").load("/cargaRapida");
    });
    //Control y Envío de Formulario
    $("#principal").bind("submit", function () {
       
        var representacion=$("#representacion").val();
        var problema=$("#problema").val();
        var problem_msg = "";
        switch (representacion) {
            case "binario":
                problem_msg = "<ul></ul>";
                switch (problema) {
                    case "sat":
                        problem_msg = B_problema_max_sat_checkValues();
                        break;
                }
                break;
            case "real":
                switch (problema) {
                    case "optimizacionFunciones":
                        problem_msg = R_pof_checkValues();
                        break;
                }
                break;
            case "permutacion":
                switch (problema) {
                    case "fssp":
                        problem_msg = P_problema_fssp_checkValues();
                        break;
                }
                break;
        }
        var init_msg = check_init_values();
        var init_flag = true;
        var problem_flag = true;
        if (init_msg != "<ul></ul>") {
            init_flag = false;
            if (problem_msg != "<ul></ul>") {
                problem_flag = false;
            }
        } else {
            if (problem_msg != "<ul></ul>") {
                problem_flag = false;
            }
        }

        //problem_flag = true //SACAR 
        if(init_flag && problem_flag){
            $('#nav-profile').load("/Resultados");
            var form_data = new FormData($('#principal')[0]);
            console.log(form_data);
            // Capturamnos el boton de envío
            var btnEnviar = $("#enviar");
            $.ajax({
                type: 'POST',
                url: '/',
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                //data:$(this).serialize(),
                beforeSend: function () {
                    /*
                    * Esta función se ejecuta durante el envió de la petición al
                    * servidor.
                    * */
                    // btnEnviar.text("Enviando"); Para button 
                    $("#loading").removeAttr("hidden");
                    btnEnviar.html("Enviando");
                    btnEnviar.attr("disabled", "disabled");
                },
                complete: function (data) {
                    /*
                    * Se ejecuta al termino de la petición
                    * */
                    btnEnviar.html("Enviar");
                    btnEnviar.removeAttr("disabled");
                    $("#loading").attr("hidden", true);
                },
                success: function (data) {

                    if (typeof (data) == "object") {

                        /*
                        * Se ejecuta cuando termina la petición y esta ha sido
                        * correcta
                        * */
                        $('#nav-home-tab').prop("hidden", false);
                        $('#nav-home-tab').prop("class", "nav-item nav-link");
                        $('#nav-home').prop("class", "tab-pane fade");
                        $('#nav-home-tab').prop("aria-selected", "false");
                        $('#nav-profile-tab').prop("hidden", false);
                        $('#nav-profile-tab').prop("class", "nav-item nav-link active");
                        $('#nav-profile').prop("class", "tab-pane fade show active");
                        $('#nav-profile-tab').prop("aria-selected", "true");
                        //Aca se obtienen los datos del serv.
                        $("#loading").attr("hidden", true);
                        //Cargamos los resultados a resultados.html
                        //Solucion
                        cargar_Resultados(data);
                        cargar_BarChart(data);
                        //$(".respuesta").html(data);

                    }

                    else if (typeof (data) == "string") {
                        errorAlert(data);
                    }
                },
                error: function (data) {
                    /*
                    * Se ejecuta si la peticón ha sido erronea
                    * * */
                    $("#loading").attr("hidden", true);
                    alert("Problemas al tratar de enviar el formulario");
                }
            });
        } else {
            var full_msg = "";
            if (init_msg != "<ul></ul>") {
                full_msg += init_msg;
            }
            if (problem_msg != "<ul></ul>") {
                full_msg += problem_msg;
            }
            //Alerta
            Swal.fire({
                title: '<h3>Atención!</h3>',
                icon: 'info',
                html: full_msg,
                showCloseButton: true,
                focusConfirm: false,
                confirmButtonText:
                    '<h4><i class="fa fa-thumbs"></i>Ok</h4>',
                confirmButtonAriaLabel: 'Ok'
            });
        }
        // Nos permite cancelar el envio del formulario
        return false;
    });
});

//Control de Valores Iniciales, antes de enviar el formulario
function check_init_values() {
    var msg = "<ul>";
    //Control de Valores Iniciales
    var init_pop = $("#poblacion_inicial").val();
    var prob_cruce = $("#probabilidad_cruce").val();
    var prob_muta = $("#probabilidad_mutacion").val();
    var lambda_hijos = $("#lambda_cantidad_de_hijos").val();
    var generaciones = $("#cantidad_generaciones").val();
    var ejecuciones = $("#cantidad_ejecuciones").val();
    if (parseInt(init_pop, 10) <= 0) {
        msg += "<li>La población Inicial NO puede ser menor o igual que 0</li>";
        $("#poblacion_inicial").css("color", "rgba(255,0,0,0.3)");
        flag = false;
    }
    if (parseFloat(prob_cruce) <= 0 || parseFloat(prob_cruce) > 1) {
        msg += "<li>La probabilidad de Cruce NO puede ser negativa, tampoco puede ser mayor a 1</li>";
        $("#probabilidad_cruce").css("background-color", "rgba(255,0,0,0.3)");
        flag = false;
    }
    if (parseFloat(prob_muta) <= 0 || parseFloat(prob_muta) > 1) {
        msg += "<li>La probabilidad de Mutación NO puede ser negativa, tampoco puede ser mayor a 1</li>";
        $("#probabilidad_mutacion").css("background-color", "rgba(255,0,0,0.3)");
        flag = false;
    }
    if (parseInt(lambda_hijos, 10) <= 0) {
        msg += "<li>La población de hijos (lambda) NO puede ser negativa, ni tampoco 0</li>";
        $("#lambda_cantidad_de_hijos").css("background-color", "rgba(255,0,0,0.3)");
        flag = false;
    } else {
        if (parseInt(lambda_hijos, 10) > parseInt(init_pop, 10)) {
            msg += "<li>La población de hijos NO puede ser mayor que la población de padres</li>";
            $("#lambda_cantidad_de_hijos").css("background-color", "rgba(255,0,0,0.3)");
            flag = false;
        }
    }
    if (parseInt(generaciones, 10) < 1) {
        msg += "<li>El Algoritmo Genético debe correr, al menos, una Generación</li>";
        $("#cantidad_generaciones").css("backgorund-color", "rgba(255,0,0,0.3)");
        flag = false;
    }
    if (parseInt(ejecuciones, 10) < 1) {
        msg += "<li>El Algoritmo Genético debe realizar, al menos, una Ejecución o Corrida</li>";
        $("#cantidad_ejecuciones").css("background-color", "rgba(255,0,0,0.3)");
        flag = false;
    }
    msg += "</ul>";
    return msg;
}
//Función que se encargar de agregar los resultados en la tabla correspondiente
//Esto es para todas las ejecuciones que se realizan
function cargar_Resultados(resultados) {

        for (var [key, instance] of Object.entries(resultados)) {

            if (key == "archivoResultado") {
                $("#botonDescargar").prop("href", instance);
            }
            else {
                var nroEjecuciones = parseInt(key, 10);
                typeof (instance)
                $("#resultados").find('tbody')
                    .append($('<tr>')
                        .append($('<th>').attr('scope', 'row').text(nroEjecuciones + 1))
                        .append($('<td>').html(instance["mejor_solucion_inicial"]))
                        .append($('<td>').html(instance["peor_solucion_inicial"]))
                        .append($('<td>').html(instance["promedio_solucion_inicial"]))
                        .append($('<td>').html(instance["mejor_solucion_final"]))
                        .append($('<td>').html(instance["peor_solucion_final"]))
                        .append($('<td>').html(instance["promedio_solucion_final"]))
                        .append($('<td>').html(instance["mejor_solucion"]))
                        .append($('<td>').html(instance["generacion_mejor_solucion"]))
                        .append($('<td>').html(instance["tiempo_mejor_solucion"]))
                        .append($('<td>').html(instance["diferencia_relativa_optimo"]))
                        .append($('<td>').html(instance["error_relativo"]))
                        .append($('<td>').html(instance["cantidad_evaluaciones_funcion_fitness"]))
                        .append($('<td>').html(instance["total_generaciones"]))
                        .append($('<td>').html(instance["tiempo_total_ejecucion"])));
            }
        }
}
//Función que sirve para cargar los resultados obtenidos
//en el Chart de Barras
function cargar_BarChart(res) {
    var i = 0;
    var cantEx = [];
    var mejorSoluciones = [];
    var mejSolI = [];
    var peorSolI = [];
    var mejSolF = [];
    var peorSolF = [];
    var optimos = [];
    var tiempo_ejecucion = [];
    var tiempo_best_solution = [];
    var diferencia_relativa = [];
    var error = [];
    for (var [key, instance] of Object.entries(res)) {
        if (key != "archivoResultado") {
            typeof (instance)
            cantEx[i] = "Ejecución " + (parseInt(key, 10) + 1);
            mejorSoluciones[i] = instance["mejor_solucion"];
            mejSolI[i] = instance["mejor_solucion_inicial"];
            peorSolI[i] = instance["peor_solucion_inicial"];
            mejSolF[i] = instance["mejor_solucion_final"];
            peorSolF[i] = instance["peor_solucion_final"];
            optimos[i] = instance["optimo"];
            tiempo_ejecucion[i] = instance["tiempo_total_ejecucion"];
            tiempo_best_solution[i] = instance["tiempo_mejor_solucion"];
            diferencia_relativa[i] = instance["diferencia_relativa_optimo"];
            error[i] = instance["error_relativo"];
            i++;
        }
    }
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = Chart.Line(ctx, {
        type: 'bar',
        data: {
            labels: cantEx,
            datasets: [
                {
                    label: 'Mejor Solución por Ejecución',
                    data: mejorSoluciones,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    fill: false,
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1
                },
                {
                    label: 'Mejor Solución Inicial',
                    backgroundColor: 'rgba(0, 191, 51, 0.2)',
                    fill: false,
                    data: mejSolI,
                    borderColor: 'rgba(0, 191, 51, 1)',
                    borderWidth: 1
                },
                {
                    label: 'Peor Solución Inicial',
                    backgroundColor: 'rgba(141, 12, 201, 0.2)',
                    fill: false,
                    data: peorSolI,
                    borderColor: 'rgba(141, 12, 201, 1)',
                    borderWidth: 1
                },
                {
                    label: 'Mejor Solución Final',
                    backgroundColor: 'rgba(242, 182, 2, 0.2)',
                    fill: false,
                    data: mejSolF,
                    borderColor: 'rgba(242, 182, 2, 1)',
                    borderWidth: 1
                },
                {
                    label: 'Peor Solución Final',
                    backgroundColor: 'rgba(242, 2, 134, 0.2)',
                    fill: false,
                    data: peorSolF,
                    borderColor: 'rgba(242, 2, 134, 1)',
                    borderWidth: 1
                },
                {
                    label: 'Óptimo Global',
                    backgroundColor: 'rgba(191, 255, 0, 0.2)',
                    fill: false,
                    data: optimos,
                    borderColor: 'rgba(191, 255, 0, 1)',
                    borderWidth: 1
                }
            ]
        },
        options: {
            responsive: true,
            hoverMode: 'index',
            stacked: false,
            tooltips: {
                mode: 'index',
                intersect: true
            },
            scales: {
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true
                    }
                }]
            }
        },
    });
    var timeCtx = document.getElementById("timeBarChart").getContext('2d');
    var timeChart = new Chart(timeCtx, {
        type: 'bar',
        data: {
            labels: cantEx,
            datasets: [
                {
                    label: 'Tiempo Total de Ejecución',
                    backgroundColor: 'rgba(0, 149, 255,0.2)',
                    borderColor: 'rgba(0, 149, 255,1)',
                    borderWidth: 1,
                    data: tiempo_ejecucion
                },
                {
                    label: 'Tiempo en que se encontró la Mejor Solución',
                    backgroundColor: 'rgba(255, 140, 0,0.2)',
                    borderColor: 'rgba(255, 140, 0,1)',
                    borderWidth: 1,
                    data: tiempo_best_solution
                }]
        },
        options: {
            responsive: true,
            hoverMode: 'index',
            stacked: false,
            tooltips: {
                mode: 'index',
                intersect: true
            },
            scales: {
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true
                    }
                }]
            }
        }
    });
    var diffCtx = document.getElementById("differenceBarChart").getContext('2d');
    var diffChart = new Chart(diffCtx, {
        type: 'horizontalBar',
        data: {
            labels: cantEx,
            datasets: [
                {
                    label: 'Solución Óptima',
                    backgroundColor: 'rgba(8, 161, 18,0.2)',
                    borderColor: 'rgba(8, 161, 18,1)',
                    borderWidth: 1,
                    data: optimos
                },
                {
                    label: 'Mejor Solución',
                    backgroundColor: 'rgba(255, 0, 0,0.2)',
                    borderColor: 'rgba(255, 0, 0,1)',
                    borderWidth: 1,
                    data: mejorSoluciones
                },
                {
                    label: 'Diferencia respecto al Óptimo',
                    backgroundColor: 'rgba(0, 94, 255,0.2)',
                    borderColor: 'rgba(0, 94, 255,1)',
                    borderWidth: 1,
                    data: diferencia_relativa
                },
                {
                    label: '% de Error',
                    backgroundColor: 'rgba(255, 204, 0,0.2)',
                    borderColor: 'rgba(255, 204, 0,1)',
                    borderWidth: 1,
                    data: error
                }
            ]
        },
        options: {
            elements: {
                rectangle: {
                    borderWidth: 2
                }
            },
            responsive: true,
            hoverMode: 'index',
            stacked: false,
            tooltips: {
                mode: 'index',
                intersect: true
            }
        }
    });
}
/*
 * Método encargado de captar el archivo cargado, realizar el control
 * respectivo, y enviarlo al servidor para su ejecución
 */
function executeConfig() {
    console.log("asdasd")
    var rep = $("#selectRepresentacion").val();
    var arch = $("#archivoConfig").prop('files');
    var formulario = null;
    //Si no hay archivo cargado, InfoAlert
    if (arch.length == 0) {
        infoAlert("Seleccione un archivo de configuracion para continuar");
    } else {
        switch (rep) {
            case '0':
                //Control del archivo de configuración Binario
                //Si esta todo Ok, retorna un formData. Sino, null
                bCheckFile(arch[0], function(res){
                    if(res==null){
                        errorAlert("El archivo de configuracion no cumple con el formato establecido");
                    }else{
                        var result=bCheckConfigValues(res);
                        if(result!=null){
                            result.append("representacion","binario");
                            sendForm(result);
                        }
                    }
                })
                break;
            case '1':
                //Control del archivo de configuración Real
                //Si está todo Ok, retorna un formData. Sino, null
                rCheckFile(arch[0], function (res) {
                    if (res == null) {
                        errorAlert("El archivo de configuracion no cumple con el formato establecido");
                    } else {
                        var result = rCheckConfigValues(res);
                        if (result != null) {
                            result.append("representacion", 'real');
                            sendForm(result);
                        }
                    }
                })
                break;
            case '2':
                //Control del archivo de configuración Permutación
                //Si está todo Ok, retorna un FormData. Sino, null
                pCheckFile(arch[0], function(res){
                    if(res==null){
                        errorAlert("El archivo de configuracion no cumple con el formato establecido");
                    }else{
                        var result=pCheckConfigValues(res);
                        if(result!=null){
                            result.append("representacion","permutacion");
                            console.log(result);
                            sendForm(result);
                        }
                    }
                })
                break;
            default:
                infoAlert("Elija una representacion para poder cargar el archivo de configuracion correspondiente");
        }
    }
}
//Método encargado del envío de formulario al Servidor
function sendForm(form) {
    $("#nav-profile").load("/Resultados");
    var btnEnviar = $("#enviar");
    $.ajax({
        type: 'POST',
        url: '/',
        data: form,
        contentType: false,
        cache: false,
        processData: false,
        //data:$(this).serialize(),
        beforeSend: function () {
            /*
            * Esta función se ejecuta durante el envió de la petición al
            * servidor.
            * */
            
            // btnEnviar.text("Enviando"); Para button 
            $("#loading").removeAttr("hidden");
            $("#loading_cargaRapida").removeAttr("hidden");
            btnEnviar.html("Enviando");
            btnEnviar.attr("disabled", "disabled");
        },
        complete: function (data) {
            /*
            * Se ejecuta al termino de la petición
            * */
            $("#fastCharge").modal('hide');
            btnEnviar.html("Enviar");
            btnEnviar.removeAttr("disabled");
            $("#loading").attr("hidden", true);
            $("#loading_cargaRapida").attr("hidden", true);
        },
        success: function (data) {
            /*
            * Se ejecuta cuando termina la petición y esta ha sido
            * correcta
            * */
            $("#fastCharge").modal('hide');
            $('#nav-home-tab').prop("hidden", false);
            $('#nav-home-tab').prop("class", "nav-item nav-link");
            $('#nav-home').prop("class", "tab-pane fade");
            $('#nav-home-tab').prop("aria-selected", "false");
            $('#nav-profile-tab').prop("hidden", false);
            $('#nav-profile-tab').prop("class", "nav-item nav-link active");
            $('#nav-profile').prop("class", "tab-pane fade show active");
            $('#nav-profile-tab').prop("aria-selected", "true");
            //Aca se obtienen los datos del serv.
            $("#loading").attr("hidden", true);
            $("#loading_cargaRapida").attr("hidden", true);
            //Cargamos los resultados a resultados.html
            //Solucion
            cargar_Resultados(data);
            cargar_BarChart(data);
            //$(".respuesta").html(data);
        },
        error: function (data) {
            /*
            * Se ejecuta si la peticón ha sido erronea
            * * */
            $("#fastCharge").modal('hide');
            $("#loading").attr("hidden", true);
            $("#loading_cargaRapida").attr("hidden", true);
            //alert("Problemas al tratar de enviar el formulario");
            errorAlert("Problemas al tratar de enviar el formulario");
        }
    })
}