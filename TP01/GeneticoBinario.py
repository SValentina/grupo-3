﻿import sys
sys.path.append(sys.path[0]+'\\Binario')
import B_operadores
import B_metodosUtiles
import abstractClass
import random
import json
import main
from time import time


class ResultadosSATBinario():
    def __init__(self):
        abstractClass.Resultados.__init__(self)

class ResultadosSATBinarioEncoder(json.JSONEncoder):
 
    def default(self, obj):
        return obj.__dict__
        


class ProblemaSATBinario:
    """
    Clase de problema SAT Binario
    """
    def __init__(self, n, conjunciones_a_evaluar):

        print ("Aca mostramos el ene " +str(n));
        self.n = n
        self.conjunciones_a_evaluar = conjunciones_a_evaluar

    def estado_aleatorio(self):
        lista = list()
        for i in range(self.n):
            lista.append(random.randint(0,1));  
        return tuple(lista)

    def costo(self, estado):
        return estado[0] + estado[-1]

    @staticmethod
    def verificar_clausula(clausula, individuo):
        for c in clausula:
            if ('-' in str(c)):
                if(individuo[abs(c)-1] == 0):
                    return 1
            elif(individuo[c-1] == 1):
                return 1
            return 0

class GeneticoBinario():

    def __init__(self, problema, n_poblacion, prob_muta, prob_cruce, nro_padres, tipo_operador_cruce,cantidad_puntos_cruce, tipo_operador_seleccion, tipo_operador_reemplazo, tipo_operador_seleccion_para_reemplazo, elitismo, lambda_cantidad_hijos):
        self.cantidad_evaluaciones_funcion_fitness = 0
        self.problema = problema
        self.prob_muta = prob_muta
        self.prob_cruce = prob_cruce
        self.nro_padres = nro_padres
        self.tipo_operador_cruce = tipo_operador_cruce
        self.cantidad_puntos_cruce = cantidad_puntos_cruce
        self.tipo_operador_seleccion = tipo_operador_seleccion
        self.tipo_operador_reemplazo = tipo_operador_reemplazo
        self.tipo_operador_seleccion_para_reemplazo = tipo_operador_seleccion_para_reemplazo
        self.lambda_cantidad_hijos = lambda_cantidad_hijos
        self.inicializa_poblacion(n_poblacion, elitismo);


    def inicializa_poblacion(self, n_poblacion, elitismo):
        """
        Inicializa la poblaci�n para el algoritmo gen�tico

        @param n_poblacion: numero de poblaci�n
        @return: None

        Internamente guarda self.npoblacion y self.poblacion

        """
        self.n_poblacion = n_poblacion
        individuos = [self.estado_a_cadena(self.problema.estado_aleatorio())
                      for _ in range(n_poblacion)]
        self.poblacion = [(self.adaptación(individuo), individuo)
                          for individuo in individuos]

        if(elitismo):
            self.posicion_mejor_individuo = self.poblacion.index(max(self.poblacion))
        else:
            self.posicion_mejor_individuo = -1


    @staticmethod
    def estado_a_cadena(estado):
        """
        Convierte un estado a una cadena de cromosomas

        @param estado: Una tupla con un estado
        @return: Una lista con una cadena de caracteres

        Por default converte el estado en una lista.

        """
        return list(estado)

    @staticmethod
    def cadena_a_estado(cadena):
        """
        Convierte una cadena de cromosomas a un estado

        @param cadena: Una lista de cromosomas o valores
        @return: Una tupla con un estado v�lido

        Por default convierte la lista a tupla

        """
        return tuple(cadena)

    def adaptación(self, individuo):
      
        self.cantidad_evaluaciones_funcion_fitness +=1
        cantidad = 0
        for clausula in self.problema.conjunciones_a_evaluar:
            if(self.problema.verificar_clausula(clausula, individuo)):
                cantidad+=1

        return cantidad/len(self.problema.conjunciones_a_evaluar)

    def busqueda(self, n_generaciones):

        resultados = ResultadosSATBinario()
        """
        Algoritmo gen�tico general

        @param n_generaciones: N�mero de generaciones a simular
        @return: Un estado del problema

        """
        ban=1
        self.solucion_encontrada = ""
        tiempo_inicial = time()
        tiempo_mejor_solucion = time()
        resultados.mejor_solucion_inicial = (max(self.poblacion)[0])
        mejor_solucion = (max(self.poblacion)[0])
        generacion_mejor_solucion=0
        print("--------------------------------")
        print("--------------------------------")
        print("Mejor de poblacion inicial:")
        print(max(self.poblacion)[0])
        print("Peor de poblacion inicial:")
        print(min(self.poblacion)[0]) 
        resultados.peor_solucion_inicial = min(self.poblacion)[0]
        print("Promedio:")
        suma = 0
        for ind in self.poblacion:
            suma = suma + ind[0]
        avg = suma / self.n_poblacion 
        resultados.promedio_solucion_inicial = avg
        print(avg) 
        print("--------------------------------")
        print("--------------------------------")
        for i in range(n_generaciones):
            #print("------GENERACION----------" + str (i))
            #print("Mejor de poblacion inicial:")
            #print(max(self.poblacion)[0])
            if (mejor_solucion < max(self.poblacion)[0]):
                mejor_solucion = max(self.poblacion)[0]
                tiempo_mejor_solucion = time()
                generacion_mejor_solucion = i
            #print("Peor de poblacion inicial:")
            #print(min(self.poblacion)[0]) 
            #print("Promedio:")
            #suma = 0
            #for ind in self.poblacion:
            #    suma = suma + ind[0]
            #avg = suma / self.n_poblacion 
            #print(avg) 
            #print("-------------------------------------------")
            #print("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW")
            if(max(self.poblacion)[0] == 1.0) and ban == 1:
                ban = 0
                self.solucion_encontrada = i
            indices_parejas = self.seleccion()
            #print ("indice de parejas")
            #print (indices_parejas)
            hijos = self.cruza(indices_parejas)
            self.mutacion(hijos)
            #print("padres")
            #print (self.indice_padres)
            self.reemplazo(hijos)
        #mas_apto = max(self.poblaci�n)
        #return self.cadena_a_estado(mas_apto[1])
        #for (ind, (aptitud, individuo)) in enumerate(self.poblacion):
        #    assert isinstance(individuo, list)
        #    print('{}: {} con {} de aptitud'.format(ind, individuo, aptitud))
        if (self.solucion_encontrada != ""):
            print ("Solucion encontrada en generaci�n: " + str(self.solucion_encontrada)+"/"+str(n_generaciones))
        tiempo_final = time()

        print("\n")
        print("\n")
        print("------Tiempo total----------")
        print(tiempo_final - tiempo_inicial)
        resultados.tiempo_total_ejecucion = tiempo_final - tiempo_inicial
        print("------Fitness mejor solucion----------")
        print(mejor_solucion)
        resultados.mejor_solucion  = mejor_solucion
        print("En generacion : " + str(generacion_mejor_solucion))
        resultados.generacion_mejor_solucion = generacion_mejor_solucion
        print("------Tiempo mejor solucion----------")
        print(tiempo_mejor_solucion - tiempo_inicial)
        resultados.tiempo_mejor_solucion = tiempo_mejor_solucion - tiempo_inicial
        print("------Cantidad de evaliaciones de funcion de fintess ------------")
        print(self.cantidad_evaluaciones_funcion_fitness)
        resultados.cantidad_evaluaciones_funcion_fitness = self.cantidad_evaluaciones_funcion_fitness
        print("------Total de generaciones ------------")
        print(n_generaciones)
        resultados.total_generaciones  = n_generaciones
        print("-----------------------:")
        print("\n")
        print("--------------------------------")
        print("--------------------------------")
        print("Mejor de poblacion final:")
        print(max(self.poblacion)[0])
        resultados.mejor_solucion_final = max(self.poblacion)[0]
        print("Peor de poblacion final:")
        resultados.peor_solucion_final = min(self.poblacion)[0]
        print(min(self.poblacion)[0]) 
        print("Promedio:")
        suma = 0
        for ind in self.poblacion:
            suma = suma + ind[0]
        avg = suma / self.n_poblacion 
        resultados.promedio_solucion_final = avg
        print(avg) 
        print("diferencia relativa con optimo")
        resultados.diferencia_relativa_optimo = abs(1-mejor_solucion)
        resultados.error_relativo = abs(1-mejor_solucion)*100
        print("error relativo")

        resultados.optimo = 1

        print("--------------------------------")
        print("--------------------------------")

        #"""resultados = {
        #    "resultados":"ejemplo",
        #}
        #resultados = json.dumps(resultados)"""
        print(resultados.__dict__)
        #resultados_serializados = json.dumps(resultados, cls=ResultadosSATBinarioEncoder, indent=4)
        #print("serial")
        #print(resultados_serializados)
        #print("serial")
        #return resultados_serializados
        return (resultados.__dict__)

   
    #def obtenerpadres(self,indices_parejas):
    #    resultado = []
    #    for padre in indices_parejas:
    #        resultado.append(self.poblacion[padre[0]])
    #        resultado.append(self.poblacion[padre[1]])

    #    return resultado
    
    def seleccion(self):

        padres=[]
        #Suma de Aptitudes de la población actual
        suma_aptitudes=1.0*sum(pop[0] for pop in self.poblacion)
        #Fitness relativo de los individuos en base a la suma de aptitudes anterior
        fitness_relativo=[(abs(pop[0]/suma_aptitudes)) for pop in self.poblacion]


        if (self.tipo_operador_seleccion == 0):
            padres = [B_operadores.seleccion_torneo_binario(self.poblacion,self.n_poblacion)
                    for _ in range(self.nro_padres)]
            self.indice_padres = padres 
            return (B_metodosUtiles.armar_parejas(padres))

        if (self.tipo_operador_seleccion == 1):
            padres = [B_operadores.seleccion_ruleta(self.poblacion,fitness_relativo)
                    for _ in range(self.nro_padres)]
            self.indice_padres = padres 
            return (B_metodosUtiles.armar_parejas(padres))

        if (self.tipo_operador_seleccion == 2):
            return (B_metodosUtiles.armar_parejas(B_operadores.seleccion_SUS(self.poblacion,self.nro_padres,fitness_relativo)))
        
    def cruza(self, ind_parejas):
        hijos = []
        #Armo las parejas normalmente mediante la seleccion y creo un vector de hijos.
        #
        if self.tipo_operador_cruce == 0:
            #primer operador
            for (i,j) in ind_parejas:
                #Recorro todas las parejas
                if random.uniform(0,1) < self.prob_cruce:
                    #si es menor a prob_cruce, entonces hago el cruce con los padres correspondientes para generar, en este caso, un hijo.
                    hijos.append(B_operadores.cruza_uniform(self,self.poblacion[i][1],
                                          self.poblacion[j][1]))
                else:
                    #sino, agrego directamente ambos padres.
                    hijos.append(self.poblacion[i][1])
                    hijos.append(self.poblacion[j][1])
                
                if(len(hijos) == self.lambda_cantidad_hijos):
                    #Aca verifico que no se vaya de tamaño el vector de hijos, por lo tanto, si se pasa borro el ultimo elemento y termino el for.
                    break
                elif (len(hijos) > self.lambda_cantidad_hijos):
                    hijos.pop()
                    break
            return hijos


        if self.tipo_operador_cruce == 1 and self.cantidad_puntos_cruce == 1:

            hijos = []
            #Armo las parejas normalmente mediante la seleccion y creo un vector de hijos.
            for (i,j) in ind_parejas:
                #Recorro todas las parejas
                if random.uniform(0,1) < self.prob_cruce:
                    #si es menor a prob_cruce, entonces hago el cruce con los padres correspondientes para generar, en este caso, un hijo.
                    hijos.append(B_operadores.cruza_single_point(self,self.poblacion[i][1],
                                            self.poblacion[j][1]))
                else:
                    #sino, agrego directamente ambos padres.
                    hijos.append(self.poblacion[i][1])
                    hijos.append(self.poblacion[j][1])
                
                if(len(hijos) == self.lambda_cantidad_hijos):
                    #Aca verifico que no se vaya de tamaño el vector de hijos, por lo tanto, si se pasa borro el ultimo elemento y termino el for.
                    break
                elif (len(hijos) > self.lambda_cantidad_hijos):
                    hijos.pop()
                    break
            return hijos

        if self.tipo_operador_cruce == 1 and self.cantidad_puntos_cruce >= 2:
            #falta

            hijos = []
            #Armo las parejas normalmente mediante la seleccion y creo un vector de hijos.
            for (i,j) in ind_parejas:
                #Recorro todas las parejas
                if random.uniform(0,1) < self.prob_cruce:
                    #si es menor a prob_cruce, entonces hago el cruce con los padres correspondientes para generar, en este caso, un hijo.
                    hijos.append(B_operadores.cruza_n_point(self,self.poblacion[i][1],
                                            self.poblacion[j][1],self.cantidad_puntos_cruce))
                else:
                    #sino, agrego directamente ambos padres.
                    hijos.append(self.poblacion[i][1])
                    hijos.append(self.poblacion[j][1])
                
                if(len(hijos) == self.lambda_cantidad_hijos):
                    #Aca verifico que no se vaya de tamaño el vector de hijos, por lo tanto, si se pasa borro el ultimo elemento y termino el for.
                    break
                elif (len(hijos) > self.lambda_cantidad_hijos):
                    hijos.pop()
                    break
            return hijos
        
    def mutacion(self, individuos):
        for individuo in individuos:
           B_operadores.mutacion_swap(self,individuo)    

    def reemplazo(self,individuos):

        if (self.tipo_operador_reemplazo == 0):
            B_operadores.reemplazo_mu_mas_lambda(self,individuos)

        if (self.tipo_operador_reemplazo == 1):
            B_operadores.reemplazo_mu_lambda(self,individuos)

        if (self.tipo_operador_reemplazo == 2):
            B_operadores.reemplazo_generacional(self,individuos)