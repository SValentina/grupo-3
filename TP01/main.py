import random
import json
import sys
import GeneticoPermutaciones
import GeneticoBinario
import GeneticoReales


def main(valores_iniciales,file):
#if __name__ == "__main__":
    data=json.loads(valores_iniciales)
    #print(data)
    res=None
    #Control de Tipo de Representación
    if(data["representacion"]=="binario"):
        res=execute_B_genetic_algorithm(data,file)
    if(data["representacion"]=="real"):
        res=execute_R_genetic_algorithm(data)
    if(data["representacion"]=="permutacion"):
        res=execute_P_genetic_algorithm(data,file)
    
    return res

def execute_B_genetic_algorithm(data,arch):
    cantidad_generaciones=data["cantidad_generaciones"]
    cantidad_ejecuciones=data["cantidad_ejecuciones"]
    lambda_cantidad_de_hijos=data["lambda_cantidad_de_hijos"]
    cantidad_padres=lambda_cantidad_de_hijos*2 #Por ahora queda asi, pero hay que generarla
    #Dependiendo del Operador de Cruce
    prob_cruce=data["probabilidad_cruce"]
    prob_mutacion=data["probabilidad_mutacion"]
    tam_poblacion=data["poblacion_inicial"]
    op_cruce=data["operador_cruce"]
    cantidad_puntos_cruce=data["cantidad_puntos_cruce"]
    op_seleccion=data["operador_seleccion"]
    op_mutacion=data["operador_mutacion"]
    op_reemplazo=data["operador_reemplazo"]
    op_seleccion_reemplazo=data["operador_seleccion_para_reemplazo"]
    elitismo=data["elitismo"]
    #Control de Archivo
    if("instancia" in data):
        instancia=data["instancia"]
        path=sys.path[0]+'\\Binario\\instances_MAXSAT_Binario\\'
        filename=path+instancia
        arch=open(filename,'r')
        line=arch.readline().split()
        #Separación de Valores de Instancia
        cantidad_clausulas=int(line[0])
        cantidad_variables=int(line[1])
        cantidad_conjunciones=int(line[2])
        #Conjunciones a Evaluar
        conjunciones_a_evaluar=[]
        for i in range(cantidad_conjunciones):
            temp=[]
            line=arch.readline().split()
            for j in range(cantidad_clausulas):
                temp.append(int(line[j]))
            conjunciones_a_evaluar.append(temp)
    elif(arch!=None):
        arch.seek(0,0)
        line=arch.readline().split()
        #Separación de Valores de Instancia
        cantidad_clausulas=int(line[0])
        cantidad_variables=int(line[1])
        cantidad_conjunciones=int(line[2])
        #Conjunciones a Evaluar
        conjunciones_a_evaluar=[]
        for i in range(cantidad_conjunciones):
            temp=[]
            line=arch.readline().split()
            for j in range(cantidad_clausulas):
                temp.append(int(line[j]))
            conjunciones_a_evaluar.append(temp)
    
    #Ejecución del Algoritmo Genético Binario
    resultado={}
    for i in range(cantidad_ejecuciones):
        genetico=GeneticoBinario.GeneticoBinario(GeneticoBinario.ProblemaSATBinario(cantidad_variables, conjunciones_a_evaluar), tam_poblacion, prob_mutacion, prob_cruce, cantidad_padres, op_cruce, cantidad_puntos_cruce, op_seleccion, op_reemplazo, op_seleccion_reemplazo, elitismo, lambda_cantidad_de_hijos)
        resultado[i] =  genetico.busqueda(cantidad_generaciones)
        #resultado.append(genetico.busqueda(cantidad_generaciones))
    #print(resultado)
    print(resultado)
    print(json.dumps(resultado))
    return json.dumps(resultado)
    #return resultado

def execute_R_genetic_algorithm(data):
    #Valores Iniciales
    init_pop=data["poblacion_inicial"]
    prob_cruce=data["probabilidad_cruce"]
    prob_mutacion=data["probabilidad_mutacion"]
    lambda_hijos=data["lambda_cantidad_de_hijos"]
    cantidad_padres=lambda_hijos*2
    cant_generaciones=data["cantidad_generaciones"]
    #Operadores para el Algoritmo Genético
    op_seleccion=data["operador_seleccion"]
    op_cruce=data["operador_cruce"]
    alfa=data["valor_alfa"]
    op_mutacion=data["operador_mutacion"]
    op_reemplazo=data["operador_reemplazo"]
    op_seleccion_reemplazo=data["operador_seleccion_para_reemplazo"]
    elitismo=data["elitismo"]
    funcion=data["funcion"]
    tam_dimension=data["tam_dimension"]
    lim_inferior=data["lim_inferior"]
    lim_superior=data["lim_superior"]
    cant_ejecuciones=data["cantidad_ejecuciones"]
    resultado={}
    for i in range(cant_ejecuciones):
        #Ejecución del Problema de Optimización de Funciones
        problema=GeneticoReales.ProblemaOptimizacionFunciones(funcion, tam_dimension, lim_inferior, lim_superior)
        genetico=GeneticoReales.GeneticoReales(problema, init_pop, cant_generaciones, cantidad_padres, op_seleccion, op_cruce, prob_cruce, op_mutacion, prob_mutacion, op_reemplazo, op_seleccion_reemplazo, elitismo, lambda_hijos, alfa)
        resultado[i]=genetico.busqueda(cant_generaciones)
    return json.dumps(resultado)


def execute_P_genetic_algorithm(data,arch):
    print(data)
    cantidad_generaciones=data["cantidad_generaciones"]
    cantidad_ejecuciones=data["cantidad_ejecuciones"]
    lambda_cantidad_de_hijos=data["lambda_cantidad_de_hijos"]
    cantidad_padres=lambda_cantidad_de_hijos*2 #Por ahora queda asi, pero hay que generarla
    #Dependiendo del Operador de Cruce
    prob_cruce=data["probabilidad_cruce"]
    prob_mutacion=data["probabilidad_mutacion"]
    tam_poblacion=data["poblacion_inicial"]
    op_cruce=data["operador_cruce"]
    op_seleccion=data["operador_seleccion"]
    op_mutacion=data["operador_mutacion"]
    op_reemplazo=data["operador_reemplazo"]
    op_seleccion_reemplazo=data["operador_seleccion_para_reemplazo"]
    elitismo=data["elitismo"]
    #Control de Archivo
    if("instancia" in data):
        instancia=data["instancia"]
        path=sys.path[0]+'\\Permutacion\\FSSPInstances\\'
        filename=path+instancia
        arch=open(filename,'r')
        line=arch.readline().split()

        no_of_jobs, no_of_machines, optimo = int(line[1]), int(line[0]), int(line[2])
        processing_time = []

        print(line)
        for i in range(no_of_jobs):
            temp = []
            line = arch.readline().split()
            for j in range(no_of_machines):
                temp.append(int(line[j]))
            processing_time.append(temp)

    elif(arch!=None):
        print("hey")
        arch.seek(0,0)
        line = arch.readline().split()
        print(line)
        # number of jobs and machines
        no_of_jobs, no_of_machines, optimo = int(line[1]), int(line[0]), int(line[2])
        # i-th job's processing time at j-th machine 
        processing_time = []
        for i in range(no_of_jobs):
            temp = []
            line = arch.readline().split()
            print(line)
            for j in range(no_of_machines):
                temp.append(int(line[j]))
            processing_time.append(temp)

        ##Separación de Valores de Instancia
        #cantidad_clausulas=int(line[0])
        #cantidad_variables=int(line[1])
        #cantidad_conjunciones=int(line[2])
        ##Conjunciones a Evaluar
        #conjunciones_a_evaluar=[]
        #for i in range(cantidad_conjunciones):
        #    temp=[]
        #    line=arch.readline().split()
        #    for j in range(cantidad_clausulas):
        #        temp.append(int(line[j]))
        #    conjunciones_a_evaluar.append(temp)
    
    #Ejecución del Algoritmo Genético Binario
    resultado={}
    for i in range(cantidad_ejecuciones):
        print("pase")
        genetico = GeneticoPermutaciones.GeneticoPermutaciones(GeneticoPermutaciones.ProblemaFSSPPermutacion(no_of_jobs, no_of_machines, processing_time), tam_poblacion, prob_mutacion, prob_cruce, cantidad_padres,op_cruce, op_seleccion, op_mutacion, op_reemplazo, op_seleccion_reemplazo, elitismo, lambda_cantidad_de_hijos, optimo)
        resultado[i] = genetico.busqueda(cantidad_generaciones)
    #resultado.append(genetico.busqueda(cantidad_generaciones))
    #print(resultado)
    #print("resultado")
    #print(resultado)
    #print(json.dumps(resultado))
    #print("finresultado")
    #return json.dumps(resultado)
    return json.dumps(resultado)
