﻿import random

def armar_parejas(padres):
    """
    armar_parejas(padres):

    Dado un array de posiciones de padres, arma tuplas que representan 
    las parejas de padres con las que luego se va a hacer el cruce

    Parameters
    ----------
    padres: vector de padres 

    Returns
    -------
    lista de tuplas donde cada tupla es una pareja de padres
    
    Example
    -------
    padres = [1,4,7,5,1,8]
    lista = [(1,4),(7,5),(1,8)]

    """
    lista = []
    j=1
    i=0
    while (j < len(padres)):
        tupla = (padres[i],padres[j])
        lista.append(tupla)
        i+=2
        j+=2
    return lista



def validarArchivosInstancia(f):
    f.seek(0,0)
    i=0
    line = f.readline().split()
    # number of jobs and machines
    no_of_jobs, no_of_machines, optimo = int(line[1]), int(line[0]), int(line[2])

    for line in f:
        i+=1
        line_split = line.split()
        if(len(line_split) == no_of_machines):
            for j in range(no_of_machines):
                if(int(line_split[j])<=0):
                    return "Solo debe haber valores positivos. Linea: " + str(i) +", Columna: " + str(j)
        else:
            return "No coincide la cantidad de máquinas informada con las encontradas en el archivo. <br>Informadas: " +str(no_of_machines)+ "<br>Se encontraron: " + str(len(line_split)) + " en la línea " + str(i)
    f.seek(0,0)
    cantidad_total_lineas = len(f.readlines())

    if(cantidad_total_lineas-1 != no_of_jobs):
        return "No coincide la cantidad de jobs con la cantidad de columnas del archivo. <br>Informados: " +str(no_of_jobs)+ "<br>Se encontraron: " + str(cantidad_total_lineas-1) 

    return ""