﻿from flask import Flask,url_for
from flask import render_template, redirect
from flask import request
import json
import sys
sys.path.append(sys.path[0]+'\\Binario')
sys.path.append(sys.path[0]+'\\Permutacion')
import B_metodosUtiles
import P_metodosUtiles
import main
from os import listdir
from flask import jsonify
import csv
from datetime import datetime
import cargaInicialDatos
import Resultados
from flask import send_from_directory

app = Flask(__name__)


"""
NOTA:
    El orden de escritura de los resultados en el achivo .csv es el siguiente:
    * Nro. de Ejecución
    * Mejor Solución Inicial
    * Peor Solución Inicial
    * Promedio Solución Inicial
    * Mejor Solución Final
    * Peor Solución Final
    * Promedio Solución Final
    * Mejor Solución
    * Generación Mejor Solución
    * Tiempo Mejor Solución
    * Diferencia Relativa al Óptimo
    * Error Relativo
    * Cantidad de Evaluaciones Función Fitness
    * Total de Generaciones
    * Tiempo Total de Ejecución
"""

CARPETA_RESULTADOS_EJECUCION = "resultados/"

@app.route('/', methods = ['GET','POST'])
def index():
    #principal_form=forms.PrincipalForm(request.form)
    if(request.method=='POST'):
        f=None     
        validacion = True

        representacion =  request.form["representacion"]
        problema = request.form["problema"]

        datos = cargaInicialDatos.cargarDatosSegunProblemaYRepresentacion(request)

        #Para manejar los archivos con instancia
        if "instancia" not in datos:
            validacion = ""
            if "archivoInstancia" in request.files:
                f=request.files["archivoInstancia"]
                if(representacion == "binario" and problema == "sat"):
                    validacion=B_metodosUtiles.validarArchivosInstancia(f)
                if(representacion == "permutacion" and problema == "fssp"):
                    validacion=P_metodosUtiles.validarArchivosInstancia(f)

                if(validacion != ""):
                    #aca hay que ver como hacemos para devolver al front el mensaje de error sin romper nada
                    #hacer validacion de resultados
                    return validacion
                    

        if(datos != None):
                
            valores_iniciales=json.dumps(datos)
            res_array=[]
            #print(valores_iniciales)
            resultados=main.main(valores_iniciales,f)
            print("flas")
            print(resultados)
            print("flas")
            resultados=json.loads(resultados)
            print("////////////////////")
            print(resultados)
            print("////////////////////")

            nombreArchivoGenerado = Resultados.generarArchivoResultado(resultados,representacion,problema)
            resultados["archivoResultado"] = nombreArchivoGenerado
        
        return resultados

    if(request.method=='GET'):
        print("Get Method")
        #print (principal_form.poblacionInicial.data)
    return render_template('index.html',urls="hola")

@app.route('/valores_iniciales')
def valores_iniciales():
    return render_template('valores_iniciales.html')

@app.route('/Binario')
def operadoresBinario():
    return render_template('operadoresBinario.html')

@app.route('/Real')
def operadoresReal():
    return render_template('operadoresReal.html')

@app.route('/Permutacion')
def operadoresPermutacion():
    return render_template('operadoresPermutacion.html')

@app.route('/cargaRapida')
def cargaRapida():
    return render_template('cargaRapida.html')

@app.route('/utils/instancias_sat_binario')
def i_sat_binario(ruta = sys.path[0] + '\\Binario\\instances_MAXSAT_Binario'):
    return jsonify(dir=listdir(ruta))    


@app.route('/utils/instancias_fssp_permutaciones')
def i_fssp_permutacion(ruta = sys.path[0] + '\\Permutacion\\FSSPInstances'):
    return jsonify(dir=listdir(ruta))    

@app.route('/Resultados')
def resultados():
    return render_template('resultados.html')

@app.route('/download')
def download():
    filename=request.args.get("filename")
    return send_from_directory("resultados", filename, as_attachment=True)

if __name__ == '__main__':

    app.run(debug = True)
    json = FlaskJSON(app)