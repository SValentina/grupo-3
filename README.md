# Grupo 3

Rivero Martini Stefanía, Scovenna Valentina, Aldana Torres - Actividad 2 (python)

ALGORITMOS GENÉTICOS (GA)

	- Metodos de seleccion: Torneo Binario, SUS
	- Metodos de reemplazo: Generacional, (mu,lambda)

	GA1:
		Selección: Torneo Binario
		Cruce: Cruzamiento Intermedio
		Reemplazo: Generacional
		Selección para reemplazo: --
		Mutación: Mutación Aleatoria Uniforme
	GA2:
		Selección: SUS
		Cruce: Cruzamiento Intermedio
		Reemplazo: (mu,lambda)
		Selección para reemplazo: --
		Mutación: Mutación Aleatoria Uniforme
 

SIMULATED ANNELING (SA)

	- Operador Unario:  Mutación por Inserción 
			    Mutación por Inversión	
